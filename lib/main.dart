import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rick_with_freezed/bloc_observable.dart';
import 'package:rick_with_freezed/ui/pages/home_page.dart';

void main() async{

  WidgetsFlutterBinding.ensureInitialized();

  final storage = await HydratedStorage.build(
      storageDirectory: await getTemporaryDirectory()
  );

  HydratedBlocOverrides.runZoned(
          () => runApp(const MyApp()),
          blocObserver: CharacterBlocObservable(),
          storage: storage
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rick and Morty',
      home: HomePage(title: 'Rick and Morty'),
    );
  }
}
