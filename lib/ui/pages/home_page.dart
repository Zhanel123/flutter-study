import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rick_with_freezed/bloc/character_bloc.dart';
import 'package:rick_with_freezed/data/repository/character_repo.dart';
import 'package:rick_with_freezed/ui/pages/search_page.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key, required this.title}) : super(key: key);

  final String title;
  final characterRepo = CharacterRepo();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black54,
        centerTitle: true,
        title: Text(
          title,
          style: Theme.of(context).textTheme.headline3,
        ),
      ),
      body: BlocProvider(
        create: (context) => CharacterBloc(characterRepo: characterRepo),
        child: Container(
            decoration: BoxDecoration(color: Colors.black87),
            child: const SearchPage()),
      ),
    );
  }
}
