import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rick_with_freezed/bloc/character_bloc.dart';
import 'package:rick_with_freezed/ui/widgets/custom_list_tile.dart';

import '../../data/models/character.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late Character _currentCharacter;
  List<Results> _currentResults = [];
  int _currentPage = 1;
  String _currentSearch = '';

  final refreshController = RefreshController();
  bool _isPagination = false;

  Timer? searchDebounce;

  final _storage = HydratedBlocOverrides.current?.storage;

  @override
  void initState() {
    if(_storage.runtimeType.toString().isEmpty) {
      if(_currentResults.isEmpty) {
        context
            .read<CharacterBloc>()
            .add(const CharacterEvent.fetch(name: '', page: 1));
      }
    }
    super.initState();
  }

  // context.watch<T>(), что заставляет виджет прослушивать изменения наT
  // context.read<T>(), который возвращает T, не слушая его

  @override
  Widget build(BuildContext context) {
    final state = context.watch<CharacterBloc>().state;

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15, bottom: 1, left: 16, right: 16),
          child: TextField(
            style: TextStyle(color: Colors.white),
            cursorColor: Colors.white,
            decoration: InputDecoration(
                filled: true,
                fillColor: Color.fromRGBO(86, 86, 86, 0.8),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide.none),
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                hintText: 'Search Name',
                hintStyle: TextStyle(color: Colors.white)),
            onChanged: (value) {
              _currentPage = 1;
              _currentResults = [];
              _currentSearch = value;

              //чтобы отправлял запрос только черещ 0.5 секунд
              searchDebounce?.cancel();
              searchDebounce = Timer(const Duration(milliseconds: 500), () {
                context
                    .read<CharacterBloc>()
                    .add(CharacterEvent.fetch(name: value, page: 1));
              });

            },
          ),
        ),
        Expanded(
          child: state.when(
              loading: () {
                if (!_isPagination) {
                  return Center(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircularProgressIndicator(),
                          SizedBox(
                            width: 10,
                          ),
                          Text('Loading...')
                        ]),
                  );
                } else {
                  return _customListView(_currentResults);
                }
              },
              loaded: (characterLoaded) {
                _currentCharacter = characterLoaded;
                if (_isPagination) {
                  _currentResults = [
                    ..._currentResults,
                    ..._currentCharacter.results
                  ];
                  refreshController
                      .loadComplete(); //значит что они готовы к отобажению
                  _isPagination = false;
                } else {
                  //иначе отображаем текущий список персонажей
                  _currentResults = _currentCharacter.results;
                }

                return _currentResults.isNotEmpty
                    ? _customListView(_currentResults)
                    : SizedBox();
              },
              error: () => const Text('Nothing found...')),
        ),
      ],
    );
    // return BlocBuilder(
    //     builder: (context, state) {
    //       if(state is CharacterStateLoading) {
    //         return ;
    //       } else if(state is CharacterStateError) {
    //         return
    //       } else if(state is CharacterStateLoaded) {
    //         return
    //       }
    //     }
    // );
  }

  Widget _customListView(List<Results> currentResults) {
    return SmartRefresher(
      controller: refreshController,
      enablePullUp: true,
      enablePullDown: false,
      onLoading: () {
        _isPagination = true;
        _currentPage++;
        if (_currentPage <= _currentCharacter.info.pages) {
          context.read<CharacterBloc>().add(
              CharacterEvent.fetch(name: _currentSearch, page: _currentPage));
        } else {
          refreshController.loadNoData();
        }
      },
      child: ListView.separated(
        itemBuilder: (context, index) {
          final result = currentResults[index];
          return Padding(
              padding: EdgeInsets.only(left: 16, right: 16, top: 3, bottom: 3),
              child: CustomListTile(result: result));
        },
        separatorBuilder: (_, index) => SizedBox(
          height: 5,
        ),
        itemCount: currentResults.length,
        shrinkWrap: true,
      ),
    );
  }
}
