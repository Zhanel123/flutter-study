import 'package:flutter/material.dart';

enum LiveState { alive, dead, unknown }

class CharacterStatus extends StatelessWidget {
  const CharacterStatus({Key? key, required this.liveStatus}) : super(key: key);
  final LiveState liveStatus;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          Icons.circle,
          size: 11,
          color: liveStatus == LiveState.alive
              ? Colors.lightGreenAccent[400]
              : liveStatus == LiveState.dead
                ? Colors.red
                : Colors.white,
        ),
        SizedBox(width: 6,),
        Text(
          liveStatus == LiveState.dead
              ? 'Dead'
              : liveStatus == LiveState.alive
                ? 'Alive'
                : 'Unknown',
          style: Theme.of(context).textTheme.bodyText1,
        )
      ],
    );
  }
}
